<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('users', ['as' => 'users.index', 'uses' => 'UsersController@index' ]);


Route::get('/', function () {
    return view('welcome1');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
