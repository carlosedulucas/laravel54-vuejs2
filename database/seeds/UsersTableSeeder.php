<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        
        User::create([
            'name'=>'Carlos',
            'email'=>'carlos@nao.com',
            'password'=>bcrypt('12345')
        ]);
        
        User::create([
            'name'=>'Eduardo',
            'email'=>'eduardo@nao.com',
            'password'=>bcrypt('12345')
        ]);  
    }
}
