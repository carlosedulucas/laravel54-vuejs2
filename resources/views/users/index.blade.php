@extends('layouts.main')

@section('content')
{{ $users }}

   <!-- component template -->
<style type="text/css">
    
    
    th {
      background-color: #42b983;
      color: rgba(255,255,255,0.66);
      cursor: pointer;
      -webkit-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
    }

      
    th.active {
      color: #FFF;
      background-color: #1A7C4F;
    }

    th.active .arrow {
      opacity: 1;
    }

    .arrow {
      display: inline-block;
      vertical-align: middle;
      width: 0;
      height: 0;
      margin-left: 5px;
      opacity: 0.66;
    }

    .arrow.asc {
      border-left: 4px solid transparent;
      border-right: 4px solid transparent;
      border-bottom: 4px solid #fff;
    }

    .arrow.dsc {
      border-left: 4px solid transparent;
      border-right: 4px solid transparent;
      border-top: 4px solid #fff;
    }

  </style>
<!-- crud root element -->
<div id="crud">
  <form id="search">
    Search <input name="query" v-model="searchQuery">
  </form>
  <crud-grid
    :data="{{ $users }}"
    :columns=JSON.parse('["id","name","email","created_at","updated_at"]')
    :filter-key="searchQuery">
  </crud-grid>
</div>
    


@stop